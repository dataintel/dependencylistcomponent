import { Component, OnInit } from '@angular/core';
import * as _ from 'underscore';

@Component({
  selector: 'app-dependency-list',
  template: ` <div class="col-md-12">
  <table  class="table table-hover table-outline mb-0 hidden-sm-down" style="margin-top: 22px;">
    <thead class="thead-default">
      <tr>
        <th style="width:10%"><i class="material-icons">format_list_bulleted</i></th>
        <th>Name</th>
      </tr>
    </thead>
    <tbody>
      <tr *ngFor ="let items of pagedItems">
        <td>{{items.id}}</td>
        <td>{{items.name}}</td>
      </tr>
    </tbody>
  </table>
</div>
 <ul *ngIf="pager.pages && pager.pages.length" class="pagination">
                <li [ngClass]="{disabled:pager.currentPage === 1}">
                    <a (click)="setPage(pager.currentPage - 1)">Prev</a>
                </li>
                <li [ngClass]="{disabled:pager.currentPage === pager.totalPages}">
                    <a (click)="setPage(pager.currentPage + 1)">Next</a>
                </li>
            </ul>`,
  styles: [`.mb-0 {
    margin-bottom: 0 !important;
}
.table-outline {
    border: 1px solid #cfd8dc;
}
.table {
    width: 100%;
    max-width: 100%;
    margin-bottom: 1rem;
}
table {
    border-collapse: collapse;
    background-color: transparent;
}

thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}

.table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #cfd8dc;
}

.thead-default th {
    color: #607d8b;
    background-color: #cfd8dc;
}

.text-muted {
    color: #b0bec5 !important;
}
`]
})
export class BaseWidgetDependencyListComponent implements OnInit {
constructor() { }
    public allItems : any[];

    // pager object
    public pager: any = {};

    // paged items
    public pagedItems: any[];
    ngOnInit() {
        this.allItems  = [
    {
      "id" : "1",
      "name" : "Sosial"
    },
    {
      "id" : "2",
      "name" : "Sosial"
    },
    {
      "id" : "3",
      "name" : "Sosial"
    },
    {
      "id" : "4",
      "name" : "Sosial"
    },
    {
      "id" : "5",
      "name" : "Sosial"
    },
    {
      "id" : "6",
      "name" : "Sosial"
    },
    {
      "id" : "7",
      "name" : "Sosial"
    },
    {
      "id" : "8",
      "name" : "Sosial"
    },
    {
      "id" : "9",
      "name" : "Sosial"
    },
    {
      "id" : "10",
      "name" : "Sosial"
    },
    {
      "id" : "11",
      "name" : "Sosial"
    },
    {
      "id" : "12",
      "name" : "Sosial"
    }
        ];
        this.setPage(1);
    }

    getPager(totalItems: number, currentPage: number = 1, pageSize: number = 8) {
            // calculate total pages
            let totalPages = Math.ceil(totalItems / pageSize);

            let startPage: number, endPage: number;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }

            // calculate start and end item indexes
            let startIndex = (currentPage - 1) * pageSize;
            let endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            let pages = _.range(startPage, endPage + 1);

            // return object with all pager properties required by the view
            return {
                totalItems: totalItems,
                currentPage: currentPage,
                pageSize: pageSize,
                totalPages: totalPages,
                startPage: startPage,
                endPage: endPage,
                startIndex: startIndex,
                endIndex: endIndex,
                pages: pages
            };
        }

    setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

}
