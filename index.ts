import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BaseWidgetDependencyListComponent } from './src/base-widget.component';

export * from './src/base-widget.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetDependencyListComponent
  ],
  exports: [
   BaseWidgetDependencyListComponent
  ]
})
export class DepedencyListModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DepedencyListModule,
      providers: []
    };
  }
}
